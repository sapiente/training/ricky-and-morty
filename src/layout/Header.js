import React, {useState} from 'react';
import {NavLink} from "react-router-dom";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons'

export default function Header() {
    const [isMobileMenuActive, setMobileMenuActive] = useState(false)
    return (
        <header>
            <div className="container">
                <nav>
                    <div className="nav-brand">
                        Ricky & Morty
                    </div>

                    <div className="menu-icons open" onClick={() => setMobileMenuActive(true)}>
                        <FontAwesomeIcon icon={faBars} />
                    </div>

                    <ul className={"nav-list " + (isMobileMenuActive ? 'active' : '')}>
                        <div className="menu-icons close" onClick={() => setMobileMenuActive(false)}>
                            <FontAwesomeIcon icon={faTimes} />

                        </div>
                        <li className="nav-item" onClick={() => setMobileMenuActive(false)}>
                            <NavLink to="/characters" className="nav-link" activeClassName="current" >Characters</NavLink>
                        </li>
                        <li className="nav-item" onClick={() => setMobileMenuActive(false)}>
                            <NavLink to="/locations" className="nav-link" activeClassName="current" >Locations</NavLink>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
    );
}