import {doGet} from "../utils/fetch";
import Location from "../models/Location";
import {getCharactersByIds} from "./charactersApi";
import {getIdFromLastSegmentOfUrl} from "../utils/functions";

export async function getLocationDetail(id) {
    const {data} = await doGet(`/api/location/${id}`);
    const residentsIds = data.residents.map(getIdFromLastSegmentOfUrl)
    data.residents = await getCharactersByIds(residentsIds);

    return Location.fromServer(data)
}